import random
import discord
from discord.ext import commands
from discord.ext.commands import Cog
import math
import platform
from helpers.checks import check_if_staff_or_ot


class Meme(Cog):
    """
    Meme commands.
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.check(check_if_staff_or_ot)
    @commands.command(hidden=True)
    async def btwiuse(self, ctx):
        """btw i use arch"""
        uname = platform.uname()
        await ctx.send(f"BTW I use {platform.python_implementation()} "
                       f"{platform.python_version()} on {uname.system} "
                       f"{uname.release}")

    @commands.check(check_if_staff_or_ot)
    @commands.command(hidden=True)
    async def yahaha(self, ctx):
        """secret command"""
        await ctx.send(f"🍂 you found me 🍂")

    @commands.check(check_if_staff_or_ot)
    @commands.command(hidden=True, aliases=["outstanding"])
    async def outstandingmove(self, ctx):
        """Posts the outstanding move meme"""
        await ctx.send("https://cdn.discordapp.com/attachments"
                       "/371047036348268545/528413677007929344"
                       "/image0-5.jpg")

    @commands.check(check_if_staff_or_ot)
    @commands.command(hidden=True)
    async def bones(self, ctx):
        await ctx.send("https://cdn.discordapp.com/emojis/"
                       "443501365843591169.png?v=1")

    @commands.check(check_if_staff_or_ot)
    @commands.command(hidden=True, aliases=["when", "etawhen",
                                            "emunand", "emummc", "thermosphere"])
    async def eta(self, ctx):
        await ctx.send("July 15.")

def setup(bot):
    bot.add_cog(Meme(bot))
